package com.huixi.auth.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.crypto.SecureUtil;
import com.huixi.auth.pojo.entity.WjUser;
import com.huixi.auth.service.WjUserService;
import com.huixi.commonutils.config.JwtConfig;
import com.huixi.commonutils.exception.BusinessException;
import com.huixi.commonutils.pojo.dto.UserTokenDTO;
import com.huixi.commonutils.util.JwtTokenUtil;
import com.huixi.commonutils.util.wrapper.ResultData;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.huixi.servicebase.base.BaseController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Optional;

/**
 * <p>
 * 用户信息表 前端控制器
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-20
 */
@RestController
@RequestMapping("/wjUser")
public class WjUserController extends BaseController {


    @Resource
    private WjUserService wjUserService;


    /**
     *  用户登录接口
     * @Author 叶秋
     * @Date 2021/9/20 16:12
     * @return com.huixi.commonutils.util.wrapper.ResultData
     **/
    @PostMapping("login")
    public ResultData login(String email, String password){

        Optional.ofNullable(email).orElseThrow(() -> new BusinessException("邮箱不能为空"));
        Optional.ofNullable(password).orElseThrow(() -> new BusinessException("密码不能为空"));

        WjUser wjUser = wjUserService.loadUserByEmail(email);

        String md5Password = SecureUtil.md5(password);
        if(!md5Password.equals(wjUser.getPassword())){
            return ResultData.error("用户名或者密码不正确");
        }

        // 生成token
        UserTokenDTO userTokenDTO = new UserTokenDTO();
        BeanUtil.copyProperties(wjUser, userTokenDTO);
        String accessToken = JwtTokenUtil.createAccessToken(userTokenDTO);

        HashMap<String, Object> map = new HashMap<>(2);
        map.put(JwtConfig.tokenHeader, accessToken);
        map.put("user", userTokenDTO);


        return ResultData.ok(map);
    }


}

