package com.huixi.auth.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.auth.pojo.entity.WjUser;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-20
 */
public interface WjUserService extends IService<WjUser> {

    /**
     * 根据用户邮箱 获取用户信息
     * @Author 叶秋
     * @Date 2021/9/20 16:07
     * @param email 邮箱
     * @return com.huixi.auth.pojo.entity.WjUser
     **/
    WjUser loadUserByEmail(String email);

}
