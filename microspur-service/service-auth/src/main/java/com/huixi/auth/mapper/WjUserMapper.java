package com.huixi.auth.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.auth.pojo.entity.WjUser;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-20
 */
public interface WjUserMapper extends BaseMapper<WjUser> {

}
