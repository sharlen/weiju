package com.huixi.auth;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 *  运行主类
 * @Author 叶秋
 * @Date 2021/9/20 16:28
 * @param
 * @return
 **/
@EnableDiscoveryClient
@MapperScan(value = "com.huixi.auth.mapper")
@ComponentScan(basePackages = {"com.huixi"})
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class AuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class, args);
    }

}
