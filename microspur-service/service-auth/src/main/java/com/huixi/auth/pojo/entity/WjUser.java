package com.huixi.auth.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.huixi.servicebase.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户信息表
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wj_user")
public class WjUser extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 用户id
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private Integer userId;

    /**
     * 昵称
     */
    @TableField("nick_name")
    private String nickName;

    /**
     * 头像对应的URL地址
     */
    @TableField("head_portrait")
    private String headPortrait;

    /**
     * 性别 改用String，活泼一点。自定义都可以
     */
    @TableField("sex")
    private String sex;

    /**
     * 手机号
     */
    @TableField("mobile")
    private String mobile;

    /**
     * 电子邮箱
     */
    @TableField("email")
    private String email;

    /**
     * 个性签名（冗余）
     */
    @TableField("signature")
    private String signature;

    /**
     * 简介
     */
    @TableField("introduce")
    private String introduce;

    /**
     * 地址
     */
    @TableField("address")
    private String address;

    /**
     * 密码（冗余）
     */
    @TableField("password")
    private String password;



}
