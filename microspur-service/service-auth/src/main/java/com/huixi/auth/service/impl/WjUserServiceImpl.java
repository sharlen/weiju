package com.huixi.auth.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.auth.mapper.WjUserMapper;
import com.huixi.auth.pojo.entity.WjUser;
import com.huixi.auth.service.WjUserService;
import com.huixi.commonutils.exception.BusinessException;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-20
 */
@Service
public class WjUserServiceImpl extends ServiceImpl<WjUserMapper, WjUser> implements WjUserService {


    /**
     * 根据用户邮箱 获取用户信息
     * @Author 叶秋
     * @Date 2021/9/20 16:07
     * @param email 邮箱
     * @return com.huixi.auth.pojo.entity.WjUser
     **/
    @Override
    public WjUser loadUserByEmail(String email) {

        Optional.ofNullable(email).orElseThrow(() -> new BusinessException("邮箱不能为空"));

        WjUser wjUser = this.lambdaQuery()
                .eq(WjUser::getEmail, email)
                .one();

        if(null == wjUser){
            throw new BusinessException("邮箱查询不到");
        }

        return wjUser;
    }
}
