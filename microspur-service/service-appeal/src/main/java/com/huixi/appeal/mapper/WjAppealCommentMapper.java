package com.huixi.appeal.mapper;

import com.huixi.appeal.pojo.entity.WjAppealComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 诉求-评论 Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
public interface WjAppealCommentMapper extends BaseMapper<WjAppealComment> {

}
