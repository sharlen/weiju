package com.huixi.appeal.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.huixi.servicebase.base.BaseController;

/**
 * <p>
 * 存储诉求的地址信息 前端控制器
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
@RestController
@RequestMapping("/wjAppealAddress")
public class WjAppealAddressController extends BaseController {

}

