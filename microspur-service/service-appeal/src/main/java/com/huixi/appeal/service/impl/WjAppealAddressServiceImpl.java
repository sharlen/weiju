package com.huixi.appeal.service.impl;

import com.huixi.appeal.pojo.entity.WjAppealAddress;
import com.huixi.appeal.mapper.WjAppealAddressMapper;
import com.huixi.appeal.service.WjAppealAddressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 存储诉求的地址信息 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
@Service
public class WjAppealAddressServiceImpl extends ServiceImpl<WjAppealAddressMapper, WjAppealAddress> implements WjAppealAddressService {

}
