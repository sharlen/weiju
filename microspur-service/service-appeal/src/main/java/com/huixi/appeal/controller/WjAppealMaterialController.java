package com.huixi.appeal.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.huixi.servicebase.base.BaseController;

/**
 * <p>
 * 诉求素材表-存储素材涉及的图片，或者大文件 前端控制器
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
@RestController
@RequestMapping("/wjAppealMaterial")
public class WjAppealMaterialController extends BaseController {

}

