package com.huixi.appeal.service;

import com.huixi.appeal.pojo.entity.WjAppealEndorse;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 此表为记录诉求点赞用途，用来判断此人是否已经点赞 服务类
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
public interface WjAppealEndorseService extends IService<WjAppealEndorse> {

}
