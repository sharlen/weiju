package com.huixi.appeal.service;

import com.huixi.appeal.pojo.entity.WjAppealAddress;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 存储诉求的地址信息 服务类
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
public interface WjAppealAddressService extends IService<WjAppealAddress> {

}
