package com.huixi.appeal.service.impl;

import com.huixi.appeal.pojo.entity.WjAppealComment;
import com.huixi.appeal.mapper.WjAppealCommentMapper;
import com.huixi.appeal.service.WjAppealCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 诉求-评论 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
@Service
public class WjAppealCommentServiceImpl extends ServiceImpl<WjAppealCommentMapper, WjAppealComment> implements WjAppealCommentService {

}
