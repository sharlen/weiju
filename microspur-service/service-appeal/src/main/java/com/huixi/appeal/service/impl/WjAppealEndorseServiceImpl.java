package com.huixi.appeal.service.impl;

import com.huixi.appeal.pojo.entity.WjAppealEndorse;
import com.huixi.appeal.mapper.WjAppealEndorseMapper;
import com.huixi.appeal.service.WjAppealEndorseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 此表为记录诉求点赞用途，用来判断此人是否已经点赞 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
@Service
public class WjAppealEndorseServiceImpl extends ServiceImpl<WjAppealEndorseMapper, WjAppealEndorse> implements WjAppealEndorseService {

}
