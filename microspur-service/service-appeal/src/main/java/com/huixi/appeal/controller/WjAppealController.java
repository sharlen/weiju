package com.huixi.appeal.controller;


import com.huixi.appeal.pojo.entity.WjAppeal;
import com.huixi.appeal.service.WjAppealService;
import com.huixi.commonutils.util.wrapper.ResultData;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.huixi.servicebase.base.BaseController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 诉求表 前端控制器
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
@RestController
@RequestMapping("/wjAppeal")
public class WjAppealController extends BaseController {


    @Resource
    private WjAppealService wjAppealService;
    
    
    /**
     *  查询所有的
     * @Author 叶秋 
     * @Date 2021/9/21 1:07
     * @return com.huixi.commonutils.util.wrapper.ResultData
     **/
    @GetMapping("all")
    public ResultData all(){

        List<WjAppeal> list = wjAppealService.list();

        return ResultData.ok(list);
    }
    

}

