package com.huixi.appeal.service.impl;

import com.huixi.appeal.pojo.entity.WjAppeal;
import com.huixi.appeal.mapper.WjAppealMapper;
import com.huixi.appeal.service.WjAppealService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 诉求表 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
@Service
public class WjAppealServiceImpl extends ServiceImpl<WjAppealMapper, WjAppeal> implements WjAppealService {

}
