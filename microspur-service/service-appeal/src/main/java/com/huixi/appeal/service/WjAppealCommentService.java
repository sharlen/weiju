package com.huixi.appeal.service;

import com.huixi.appeal.pojo.entity.WjAppealComment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 诉求-评论 服务类
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
public interface WjAppealCommentService extends IService<WjAppealComment> {

}
