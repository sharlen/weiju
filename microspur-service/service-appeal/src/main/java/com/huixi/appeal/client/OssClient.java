package com.huixi.appeal.client;

import com.huixi.appeal.client.errorImpl.OssClientImpl;
import com.huixi.commonutils.util.wrapper.ResultData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *  阿里oss存储库 相关接口
 * @author 叶秋
 * @date 2021/4/11 21:00
 **/
@Component
@FeignClient(name="service-oss",fallback = OssClientImpl.class)
public interface OssClient {

    
    /**
     *  oss服务的测试类
     * @author 叶秋
     * @date 2021/4/11 21:07
     * @return com.huixi.commonutils.util.wrapper.ResultData
     **/
    @GetMapping("/service-oss/helloOss")
    ResultData helloOss();

}
