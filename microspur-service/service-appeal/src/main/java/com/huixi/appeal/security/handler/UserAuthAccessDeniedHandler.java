package com.huixi.appeal.security.handler;


import com.huixi.commonutils.errorcode.ErrorCodeEnum;
import com.huixi.commonutils.util.ResultUtil;
import com.huixi.commonutils.util.wrapper.ResultData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Description 暂无权限处理类
 * @Author Sans
 * @CreateTime 2019/10/3 8:39
 */
@Slf4j
@Component
public class UserAuthAccessDeniedHandler implements AccessDeniedHandler {

    /**
     * 暂无权限返回结果
     * @Author Sans
     * @CreateTime 2019/10/3 8:41
     */
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException exception){

        ResultUtil.responseJson(response, ResultData.error(ErrorCodeEnum.FORBIDDEN));

    }


}
