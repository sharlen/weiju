package com.huixi.appeal.client.errorImpl;

import com.huixi.appeal.client.OssClient;
import com.huixi.commonutils.util.wrapper.ResultData;
import org.springframework.stereotype.Component;

/**
 *  oss实现报错时 提供默认的返回值
 * @Author 叶秋 
 * @Date 2021/4/11 21:03
 * @param 
 * @return 
 **/
@Component
public class OssClientImpl implements OssClient {
    /**
     * oss服务的测试类
     *
     * @return com.huixi.commonutils.util.wrapper.ResultData
     * @Author 叶秋
     * @Date 2021/4/11 21:07
     **/
    @Override
    public ResultData helloOss() {
        return ResultData.error();
    }
}
