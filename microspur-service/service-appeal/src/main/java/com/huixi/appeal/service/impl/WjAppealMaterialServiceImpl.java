package com.huixi.appeal.service.impl;

import com.huixi.appeal.pojo.entity.WjAppealMaterial;
import com.huixi.appeal.mapper.WjAppealMaterialMapper;
import com.huixi.appeal.service.WjAppealMaterialService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 诉求素材表-存储素材涉及的图片，或者大文件 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
@Service
public class WjAppealMaterialServiceImpl extends ServiceImpl<WjAppealMaterialMapper, WjAppealMaterial> implements WjAppealMaterialService {

}
