package com.huixi.appeal.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.huixi.servicebase.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 诉求素材表-存储素材涉及的图片，或者大文件
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wj_appeal_material")
public class WjAppealMaterial extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 诉求素材表的id
     */
    @TableId(value = "appeal_material_id", type = IdType.AUTO)
    private Integer appealMaterialId;

    /**
     * 诉求id
     */
    @TableField("appeal_id")
    private Integer appealId;

    /**
     * 素材的url地址（存放在OSS）
     */
    @TableField("url")
    private String url;



}
