package com.huixi.appeal.mapper;

import com.huixi.appeal.pojo.entity.WjAppealMaterial;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 诉求素材表-存储素材涉及的图片，或者大文件 Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
public interface WjAppealMaterialMapper extends BaseMapper<WjAppealMaterial> {

}
