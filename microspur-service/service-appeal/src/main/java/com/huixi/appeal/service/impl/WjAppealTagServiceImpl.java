package com.huixi.appeal.service.impl;

import com.huixi.appeal.pojo.entity.WjAppealTag;
import com.huixi.appeal.mapper.WjAppealTagMapper;
import com.huixi.appeal.service.WjAppealTagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 诉求-对应标签 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
@Service
public class WjAppealTagServiceImpl extends ServiceImpl<WjAppealTagMapper, WjAppealTag> implements WjAppealTagService {

}
