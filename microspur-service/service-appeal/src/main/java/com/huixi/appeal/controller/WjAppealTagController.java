package com.huixi.appeal.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.huixi.servicebase.base.BaseController;

/**
 * <p>
 * 诉求-对应标签 前端控制器
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
@RestController
@RequestMapping("/wjAppealTag")
public class WjAppealTagController extends BaseController {

}

