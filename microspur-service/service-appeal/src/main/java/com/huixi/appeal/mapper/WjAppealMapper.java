package com.huixi.appeal.mapper;

import com.huixi.appeal.pojo.entity.WjAppeal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 诉求表 Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
public interface WjAppealMapper extends BaseMapper<WjAppeal> {

}
