package com.huixi.appeal.mapper;

import com.huixi.appeal.pojo.entity.WjAppealEndorse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 此表为记录诉求点赞用途，用来判断此人是否已经点赞 Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
public interface WjAppealEndorseMapper extends BaseMapper<WjAppealEndorse> {

}
