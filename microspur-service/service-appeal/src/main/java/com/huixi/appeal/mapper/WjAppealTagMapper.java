package com.huixi.appeal.mapper;

import com.huixi.appeal.pojo.entity.WjAppealTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 诉求-对应标签 Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
public interface WjAppealTagMapper extends BaseMapper<WjAppealTag> {

}
