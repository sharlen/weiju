package com.huixi.appeal.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.huixi.servicebase.base.BaseController;

/**
 * <p>
 * 此表为记录诉求点赞用途，用来判断此人是否已经点赞 前端控制器
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
@RestController
@RequestMapping("/wjAppealEndorse")
public class WjAppealEndorseController extends BaseController {

}

