package com.huixi.appeal.mapper;

import com.huixi.appeal.pojo.entity.WjAppealAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 存储诉求的地址信息 Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2021-09-21
 */
public interface WjAppealAddressMapper extends BaseMapper<WjAppealAddress> {

}
