package com.huixi.appeal;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;


/**
 *  运行主类
 * @Author 叶秋 
 * @Date 2021/9/20 23:55
 * @param 
 * @return 
 **/
@EnableFeignClients
@EnableCaching
@EnableAsync
@EnableTransactionManagement
@EnableDiscoveryClient
@SpringBootApplication
@MapperScan(value = "com.huixi.appeal.mapper")
@ComponentScan(basePackages = {"com.huixi"})
public class AppealApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppealApplication.class, args);
    }
    
}
