package com.huixi.oss.security.handler;

import com.huixi.commonutils.config.JwtConfig;
import com.huixi.commonutils.pojo.dto.UserTokenDTO;
import com.huixi.commonutils.util.JwtTokenUtil;
import com.huixi.commonutils.util.ResultUtil;
import com.huixi.commonutils.util.wrapper.ResultData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description 登录成功处理类
 * @Author Sans
 * @CreateTime 2019/10/3 9:13
 */
@Slf4j
@Component
public class UserLoginSuccessHandler implements AuthenticationSuccessHandler {


    /**
     * 登录成功返回结果
     * @Author Sans
     * @CreateTime 2019/10/3 9:27
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication){
        // 组装JWT
        UserTokenDTO userTokenDTO =  (UserTokenDTO) authentication.getPrincipal();

        String token = JwtTokenUtil.createAccessToken(userTokenDTO);
        token = JwtConfig.tokenPrefix + token;

        Map<String, Object> map = new HashMap<>(2);
        map.put("token", token);
        map.put("user", userTokenDTO);

        ResultUtil.responseJson(response, ResultData.ok(map));


    }


}
