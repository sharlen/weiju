package com.huixi.oss.controller;

import com.huixi.commonutils.errorcode.ErrorCodeEnum;
import com.huixi.commonutils.exception.BusinessException;
import com.huixi.commonutils.util.wrapper.ResultData;
import com.huixi.servicebase.util.SecurityUtil;
import com.netflix.hystrix.contrib.javanica.utils.CommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 *  上传阿里oss 存储库 相关接口
 * @Author 叶秋 
 * @Date 2021/4/10 14:45
 **/
@RestController
@Slf4j
public class OssController {



    /**
     *  测试
     * @Author 叶秋
     * @Date 2021/4/10 14:49
     * @return void
     **/
    @GetMapping(value = "helloOss")
    public ResultData helloOss(){
        log.info("hello oss");

        Integer nowUserId = SecurityUtil.getNowUserId();

        System.out.println(nowUserId);

        return ResultData.ok("hello oss");
    }

}
