package com.huixi.generator;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: microspur
 * @className: MyBatisPlusGenerator
 * @author: 徐子楼
 * @date: 2020-01-16 16:30
 * @description: 代码生成器
 **/
public class MyBatisPlusGenerator {


    public static void main(String[] args) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        //配置 GlobalConfig
        GlobalConfig globalConfig = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        globalConfig.setOutputDir("C:\\Users\\yeqiu\\Desktop\\huixi\\weiju\\microspur-service\\service-appeal\\src\\main\\java");
        globalConfig.setAuthor("叶秋");
        globalConfig.setOpen(false);
        globalConfig.setSwagger2(false);
        globalConfig.setMapperName("%sMapper");
        globalConfig.setXmlName("%sMapper");
        globalConfig.setServiceName("%sService");
        globalConfig.setServiceImplName("%sServiceImpl");
        globalConfig.setControllerName("%sController");
        mpg.setGlobalConfig(globalConfig);
        //配置 DataSourceConfig
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setUrl("jdbc:mysql://47.106.225.52:3306/weiju?useUnicode=true&characterEncoding=UTF-8");
        dataSourceConfig.setDriverName("com.mysql.cj.jdbc.Driver");
        dataSourceConfig.setUsername("root");
        dataSourceConfig.setPassword("weiju");
        mpg.setDataSource(dataSourceConfig);
        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        // 此处可以修改为您的表前缀
//        strategy.setTablePrefix("sys_");
        // 表名生成策略
        strategy.setNaming(NamingStrategy.underline_to_camel);
//        strategy.setNotLikeTable(new LikeTable("sys_"));
//        strategy.setLikeTable(new LikeTable("sys_"));
        // 需要生成的表
        strategy.setInclude("wj_appeal", "wj_appeal_address", "wj_appeal_comment", "wj_appeal_endorse",
                "wj_appeal_material", "wj_appeal_tag");
        // 排除生成的表
//        strategy.setExclude(new String[]{"tb_pay_log", "tb_order_item", "tb_order",
//                "tb_user", "tb_content_category", "tb_content",
//                "tb_seller"});
        // 自定义实体父类
        strategy.setSuperEntityClass("com.huixi.servicebase.base.BaseEntity");
        // 自定义实体，公共字段
        // strategy.setSuperEntityColumns(new String[] { "test_id", "age" });
        // 自定义 mapper 父类
//        strategy.setSuperMapperClass("com.huixi.servicebase.base.BaseMapper");
        // 自定义 com.lg.product.service.service 父类
        // strategy.setSuperServiceClass("com.baomidou.demo.TestService");
        // 自定义 com.lg.product.service.service 实现类父类
        // strategy.setSuperServiceImplClass("com.baomidou.demo.TestServiceImpl");
        // 自定义 com.lg.product.web.com.lg.product.web.controller 父类
        strategy.setSuperControllerClass("com.huixi.servicebase.base.BaseController");
        // 生成 RestController 风格
        strategy.setRestControllerStyle(true);
        strategy.setEntityLombokModel(true);
        strategy.setEntityTableFieldAnnotationEnable(true);
        mpg.setStrategy(strategy);
        // 包配置
        // 注意不同的模块生成时要修改对应模块包名
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.huixi.appeal");
        pc.setEntity("pojo.entity");
        pc.setMapper("mapper");
        pc.setXml("mapper.xml");
        pc.setService("service");
        pc.setServiceImpl("service.impl");
        pc.setController("controller");
        mpg.setPackageInfo(pc);
        // 注入自定义配置，可以在 VM 中使用 cfg.abc 设置的值
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                Map<String, Object> map = new HashMap<>();
                map.put("abc", this.getConfig().getGlobalConfig().getAuthor() + "-mp");
                this.setMap(map);
            }
        };
        mpg.setCfg(cfg);
        // 自定义模板配置，可以 copy 源码 mybatis-plus/src/main/resources/templates 下面内容修改，
        // 放置自己项目的 src/main/resources/templates 目录下, 默认名称一下可以不配置，也可以自定义模板名称
        TemplateConfig tc = new TemplateConfig();
        tc.setController("/templates/controller.java.vm");
        tc.setEntity("/templates/entity.java.vm");
        tc.setMapper("/templates/mapper.java.vm");
        tc.setXml("/templates/mapper.xml.vm");
        tc.setService("/templates/service.java.vm");
        tc.setServiceImpl("/templates/serviceImpl.java.vm");
        // 如上任何一个模块如果设置 空 OR Null 将不生成该模块。
        mpg.setTemplate(tc);
        // 执行生成
        mpg.execute();

    }
}
