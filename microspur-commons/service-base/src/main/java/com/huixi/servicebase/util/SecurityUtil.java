package com.huixi.servicebase.util;


import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *  SpringSecurity相关的帮助类
 * @Author 叶秋 
 * @Date 2021/9/20 17:09
 * @param 
 * @return 
 **/
public class SecurityUtil {


    /**
     *  获取现在正在 调用这个接口的用户id
     * @Author 叶秋
     * @Date 2020/6/18 23:21
     * @return java.lang.String
     **/
    public static Integer getNowUserId(){

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String s = authentication.getCredentials().toString();
        Integer userId = Integer.valueOf(s);

        return userId;

    }


}
