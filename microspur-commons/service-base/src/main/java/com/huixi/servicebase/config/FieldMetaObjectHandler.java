package com.huixi.servicebase.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.huixi.commonutils.constant.Constant;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;

/**
 *  公共字段
 * @Author 叶秋 
 * @Date 2021/9/19 21:51
 * @param 
 * @return 
 **/
@Component
public class FieldMetaObjectHandler implements MetaObjectHandler {

    private final static String CREATE_TIME = "createTime";
    private final static String UPDATE_TIME = "updateTime";
    private final static String DEL_FLAG = "delFlag";
    private final static String STATUS_FLAG = "statusFlag";
    private final static String VERSION = "version";

    @Override
    public void insertFill(MetaObject metaObject) {

        LocalDateTime localDateTime = LocalDateTime.now();

        //创建时间
        setFieldValByName(CREATE_TIME, localDateTime, metaObject);

        //更新时间
        setFieldValByName(UPDATE_TIME, localDateTime, metaObject);

        //删除标识
        setFieldValByName(DEL_FLAG, Constant.FALSE, metaObject);

        //状态码
        setFieldValByName(STATUS_FLAG, Constant.STATUS_DEFAULT, metaObject);

        // 乐观锁
        setFieldValByName(VERSION, Constant.VERSION_DEFAULT, metaObject);

    }

    @Override
    public void updateFill(MetaObject metaObject) {

        LocalDateTime localDateTime = LocalDateTime.now();

        //更新时间
        setFieldValByName(UPDATE_TIME, localDateTime, metaObject);
    }
}