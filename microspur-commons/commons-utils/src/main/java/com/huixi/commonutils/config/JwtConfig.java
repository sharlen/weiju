package com.huixi.commonutils.config;

import lombok.Getter;

/**
 * JWT配置类
 * @Author Sans
 * @CreateTime 2019/10/1 22:56
 */
@Getter
public class JwtConfig {
    /**
     * 密钥KEY
     */
    public static String secret = "weiju-good";
    /**
     * TokenKey
     */
    public static String tokenHeader = "token";
    /**
     * Token前缀字符
     */
    public static String tokenPrefix = "weiju-";
    /**
     * 过期时间 单位秒 1天后过期=86400 7天后过期=604800
     */
    public static Integer expiration = 604800;
    /**
     * 不需要认证的接口
     */
    public static String antMatchers = "/login,/loginOut";

}