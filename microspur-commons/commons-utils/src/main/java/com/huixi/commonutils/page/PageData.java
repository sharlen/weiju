package com.huixi.commonutils.page;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页查询最终传过去的值
 * @Author 叶秋
 * @Date 2020/6/2 21:56
 * @param
 * @return
 **/
@Data
public class PageData<T> implements Serializable {

    private static final long serialVersionUID = 1L;


    private List<T> records;

    private Integer total;

    private Integer size;

    private Integer current;


}