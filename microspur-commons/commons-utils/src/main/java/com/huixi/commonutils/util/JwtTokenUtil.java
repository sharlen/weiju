package com.huixi.commonutils.util;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.huixi.commonutils.config.JwtConfig;
import com.huixi.commonutils.pojo.dto.UserTokenDTO;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * JWT工具类
 * @Author Sans
 * @CreateTime 2019/10/2 7:42
 */
@Slf4j
public class JwtTokenUtil {


    /**
     * 生成Token
     * @Author Sans
     * @CreateTime 2019/10/2 12:16
     * @Param  selfUserEntity 用户安全实体
     * @Return Token
     */
    public static String createAccessToken(UserTokenDTO userTokenDTO){
        // 登陆成功生成JWT
        String token = Jwts.builder()
                // 放入用户名和用户ID
                .setId(userTokenDTO.getUserId() + "")
                // 主题
                .setSubject(userTokenDTO.getNickName())
                // 签发时间
                .setIssuedAt(new Date())
                // 签发者
                .setIssuer("weiju")
                // 自定义属性 放入用户拥有权限
//                .claim("authorities", JSON.toJSONString(querySelfInfoVO.getAuthorities()))
                .claim("user", JSONUtil.toJsonStr(userTokenDTO))
                // 失效时间
                .setExpiration(new Date(System.currentTimeMillis() + JwtConfig.expiration))
                // 签名算法和密钥
                .signWith(SignatureAlgorithm.HS512, JwtConfig.secret)
                .compact();
        return JwtConfig.tokenPrefix + token;
    }



}
