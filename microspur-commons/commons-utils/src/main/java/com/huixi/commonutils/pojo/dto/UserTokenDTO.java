package com.huixi.commonutils.pojo.dto;


import lombok.Data;

/**
 *  用户token信息的类
 * @Author 叶秋 
 * @Date 2021/9/20 14:49
 * @param 
 * @return 
 **/
@Data
public class UserTokenDTO {


    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 昵称
     */
    private String nickName;


}
